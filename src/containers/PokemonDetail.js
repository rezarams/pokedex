import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { useParams, useHistory } from "react-router-dom";
import { GET_POKEMON } from '../graphql/get-pokemon';
import "@fontsource/red-hat-display";
import styled from '@emotion/styled';
import BackArrow from '../img/back-button.svg';

const TypeColor = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 87px;
  height: 27px;
  border-radius: 16px;
  font-size: 14px;
  font-weight: bold;
  color: white;
  margin: 4px;
`

const TypeGrid = styled.div`
  position: absolute;
  right: 20px;
  top: 6px;
`

const PokeName = styled.h1`
  font-size: 32px;
  font-weight: bold;
  margin: 0px;
`

const PokeNum = styled.p`
  color: #AFAFAF;
  margin: 0px;
`

const PokeHeader = styled.div`
  margin-left: 20px;
  #normal {
    background-color: #A8A878;
  }
  
  #fire {
    background-color: #F08030;
  }
  
  #water {
    background-color: #6890F0;
  }
  
  #electric {
    background-color: #F8D030;
  }
  
  #grass {
    background-color: #78C850;
  }
  
  #ice {
    background-color: #98D8D8;
  }
  
  #ground {
    background-color: #E0C068;
  }
  
  #flying {
    background-color: #A890F0;
  }
  
  #ghost {
    background-color: #705898;
  }
  
  #rock {
    background-color: #B8A038;
  }
  
  #fighting {
    background-color: #C03028;
  }
  
  #poison {
    background-color: #A040A0;
  }
  
  #psychic {
    background-color: #F85888;
  }
  
  #bug {
    background-color: #A8B820;
  }
  
  #dark {
    background-color: #705848;
  }
  
  #steel {
    background-color: #B8B8D0;
  }
  
  #dragon {
    background-color: #7038F8;
  }
`

const HeaderGrid = styled.div`
  position: relative;
`

const ContentWrapper = styled.div`
  font-family: Red Hat Display;
  img {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 178px;
    height: 184px;
    margin-top: 10px;
  }
`

const MovesCard = styled.div`
  display: block;
  margin-left: auto;
  margin-right: auto;
  background: #FFFFFF;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.15);
  border-radius: 8px;
  width: 312px;
  height: 210px;
`

const MovesTitle = styled.p`
  font-weight: bold;
  margin: 0px;
  padding-top: 8px;
  padding-left: 16px;
  padding-right: 16px;
`

const MovesLine = styled.div`
  border-bottom: 1px solid;
  border-color: #8A8A8A;
  margin: 5px 16px 5px 16px;
`

const MoveList = styled.div`
  height: 162px;
  width: 280px;
  overflow-y: scroll;
  margin: 5px 16px 5px 16px;
`

const MoveName = styled.div`
  background-color: #F7F7F7;
  border-radius: 16px;
  margin: 6px 0px 6px 0px;
`

const CatchButton = styled.button`
  font-family: Red Hat Display;
  display: block;
  margin: 16px auto 16px auto;
  width: 312px;
  height: 48px;
  background-color: #F7FC04;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.15);
  border-radius: 8px;
  font-size: 20px;
  font-weight: bold;
  outline: none;
  border-width: 0px;
  :hover {
    background-color: #D8DC2D;
  }
`

const Navbar = styled.div`
  position: relative;
  width: 100%;
  height: 72px;
`

const Back = styled.img`
  position: absolute;
  top: 24px;
  left: 34px;
`

const Details = styled.div`
  position: relative;
`

function PokemonHeader(props) {
  let num = `${props.pokemon.id}`;
  let arr = num.split(".")
  arr[0] = arr[0].padStart(3, "0");
  let str = arr.join(".");

  let name = props.pokemon.name.charAt(0).toUpperCase() + props.pokemon.name.slice(1);

  return (
    <PokeHeader>
      <PokeNum>#{str}</PokeNum>
      <HeaderGrid>
        <PokeName>{name}</PokeName>
        <TypeGrid>
          {props.pokemon.types.map((type) => <TypeColor id={type.type.name} key={type.type.name}>{type.type.name}</TypeColor>)}
        </TypeGrid>
      </HeaderGrid>
    </PokeHeader>
  )
}

function PokemonMoves(props) {
  return (
    <MovesCard>
      <MovesTitle>Moves</MovesTitle>
      <MovesLine />
      <MoveList>
        {props.pokemon.moves.map((move) => <MoveName key={move.move.name}>{move.move.name}</MoveName>)}
      </MoveList>
    </MovesCard>
  )
}

export function PokemonDetail() {
  const history = useHistory();
  const { name } = useParams();

  const { loading, data } = useQuery(GET_POKEMON, {
    variables: { name: name },
  });

  if(loading) return <p>Loading...</p>

  console.log(data);

  const routeChange = () => {
    let path = `/`;
    history.push(path);
  }

  return (
    <Details>
      <Navbar>
        <Back src={BackArrow} onClick={routeChange}/>
      </Navbar>
      <ContentWrapper>
        <PokemonHeader {...data} />
        <img src={data.pokemon.sprites.front_default} alt="pokemon_sprites"/>
        <PokemonMoves {...data} />
        <CatchButton onClick={() => console.log('pressed')}>Catch 'Em!</CatchButton>
      </ContentWrapper>
    </Details>
  )
}
