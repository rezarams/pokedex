import React from 'react';
import styled from '@emotion/styled';
import { useQuery } from '@apollo/react-hooks';
import { GET_POKEMONS } from '../graphql/get-pokemons';
import Pokemon from '../components/Pokemon';
import "@fontsource/red-hat-display";

const LoadButton = styled.button`
  font-family: Red Hat Display;
  display: block;
  margin-left: auto;
  margin-right: auto;
  border: 0px;
  padding: 0px;
  margin-bottom: 24px;
  width: 312px;
  font-weight: bold;
  border-radius: 8px;
  color: white;
  background-color: #2A7AF9;
`

const CardWrapper = styled.div`
  content: "";
  display: table;
  clear: both;
`

const PokeList = styled.div`
  position: relative;
`

export function PokemonsContainer() {
  const { loading, data: { pokemons = {} } = {}, fetchMore } = useQuery(GET_POKEMONS, {
    variables: { limit: 19, offset: 0 },
  });

  if(loading) return <p>Loading...</p>;

  return (
    <PokeList>
      <CardWrapper>
        {pokemons.results && pokemons.results.map(pokemon => <Pokemon key={pokemon.name} pokemon={pokemon}/>)}
      </CardWrapper>
      <LoadButton onClick={() => 
      fetchMore({
        variables: { offset: pokemons.nextOffset },
        updateQuery: (prevResult, {fetchMoreResult}) => {
          fetchMoreResult.pokemons.results = [
            ...prevResult.pokemons.results,
            ...fetchMoreResult.pokemons.results
          ]
          return fetchMoreResult;
        }
      })
      }>
        Load more
      </LoadButton>
    </PokeList>
  )
}
