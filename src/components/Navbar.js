import React, { useState } from 'react'
import "@fontsource/red-hat-display";
import styled from '@emotion/styled';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom';

const NavbarWrapper = styled.nav`
  display: flex;
  font-family: Red Hat Display;
  background-color: #EE4540;
  height: 72px;
  padding: 0px;
  position: fixed;
  width: 100%;
  z-index: 9999;
`

const NavTitle = styled.h1`
  color: white;
  font-size: 22px;
  font-weight: bold;
  margin: 0px;
  padding-top: 22px;
  padding-left: 64px;
`

const NavButton = styled.button`
  position: absolute;
  color: white;
  background-color: #EE4540;
  border: 0px;
  padding: 0px;
  .burger-icon {
    width: 24px;
    height: 24px;
  }
  top: 24px;
  left: 24px;
`

const ListLink = styled.ul`
  position: absolute;
  z-index: 9999;
  display: block;
  list-style: none;
  background-color: #EE4540;
  left: 0;
  top: 72px;
  transition: all 0.5s ease-out;
  width: 100%;
  margin: 0px;
  padding: 0px;
`

const ListName = styled.li`
  color: white;
  display: block;
  margin-left: auto;
  margin-right: auto;
  padding: 20px;
  transition: all 0.5s ease;
`

export default function Navbar() {
  const [isHidden, setIsHidden] = useState(false)
  return (
    <NavbarWrapper>
      <NavButton onClick={() => setIsHidden(!isHidden)}>
        <FontAwesomeIcon className="burger-icon" icon={faBars} />
      </NavButton>
      <NavTitle>PokeDex!</NavTitle>
      {isHidden && 
      <ListLink>
        <Link to="/">
          <ListName>Pokedex</ListName>
        </Link>
        <Link>
          <ListName>Pokebank</ListName>
        </Link>
      </ListLink>}
    </NavbarWrapper>
  )
}
