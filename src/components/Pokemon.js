import React from 'react';
import styled from '@emotion/styled';
import { useHistory } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { GET_POKEMON } from '../graphql/get-pokemon';
import "@fontsource/red-hat-display";
import Pokeball from "../img/pokeball.svg";

const ListCard = styled.div`
  font-family: Red Hat Display;
  float: left;
  margin: 5px;
`

const PokeCard = styled.div`
  position: relative;
  background: #FFFFFF;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.15);
  border-radius: 8px;
  margin: 15px;
  width: 146px;
  height: 101px;
`

const PokeSprite = styled.img`
  width: 60px;
  height: 62px;
  position: absolute;
  top: 24px;
  right: 0px;
`

const PokeBall = styled.img`
  position: absolute;
  top: 8px;
  right: 8px;
`

const PokeName = styled.h3`
  font-size: 16px;
  font-weight: bold;
  margin: 0px;
  padding-left: 8px;
  padding-top: 8px;
`

const PokeNum = styled.div`
  color: #8A8A8A;
  font-size: 12px;
  margin: 0px;
  padding-left: 8px;
`

const OwnedPoke = styled.div`
  color: #8A8A8A;
  font-size: 12px;
  padding-left: 8px;
  padding-top: 32px;
`

function PokemonHeader(props) {
  const { loading, data } = useQuery(GET_POKEMON, {
    variables: { name: props.name },
  });

  if(loading) return <p>Loading...</p>

  let num = `${data.pokemon.id}`;
  let arr = num.split(".")
  arr[0] = arr[0].padStart(3, "0");
  let str = arr.join(".");

  let name = props.name.charAt(0).toUpperCase() + props.name.slice(1);

  return (
    <div>
      <PokeName>{name}</PokeName>
      <PokeNum>#{str}</PokeNum>
    </div>
  )
}

const Pokemon = ( { pokemon } ) => {
  const history = useHistory();

  const routeChange = () => {
    let path = `/detail/${pokemon.name}`;
    history.push(path);
  }

  return (
    <ListCard className="pokemon" onClick={routeChange}>
      <PokeCard>
        <PokemonHeader {...pokemon} />
        <PokeSprite src={pokemon.image} alt={pokemon.name}/>
        <PokeBall src={Pokeball} />
        <OwnedPoke>Owned: 0</OwnedPoke>
      </PokeCard>
    </ListCard>
  )
}

export default Pokemon;
