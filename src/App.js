import React from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import { PokemonsContainer } from "./containers/PokemonsContainer";
import { PokemonDetail } from "./containers/PokemonDetail";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import styled from '@emotion/styled';

const EmptySpace = styled.div`
  background: white;
  padding: 0;
  height: 72px;
`

function App() {
  const client = new ApolloClient({
    uri: 'https://graphql-pokeapi.vercel.app/api/graphql'
  })

  return (
    <ApolloProvider client={client}>
      <Router>
        <Navbar />
        <EmptySpace />
        <Switch>
          <Route path="/" exact component={PokemonsContainer} />
          <Route path="/detail/:name" component={PokemonDetail} />
        </Switch>
      </Router>
    </ApolloProvider>
  );
}

export default App;
